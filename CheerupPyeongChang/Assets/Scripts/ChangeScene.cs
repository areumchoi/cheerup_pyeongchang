﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class ChangeScene : MonoBehaviour {
	public GameObject panel;

	public void SceneChange_AR(){
		string before_string;
		string before_check;
		string before_;
		string now_time = System.DateTime.Now.ToString ("yyyyMMddHHmmss");
		long now = (long)Convert.ChangeType(now_time, typeof(long));
		before_string = PlayerPrefs.GetString ("before_string","0");
		before_check = PlayerPrefs.GetString ("before_check","0");
		before_ = PlayerPrefs.GetString ("before_","0");
		long before = (long)Convert.ChangeType(before_string, typeof(long));
		long before_plus = (long)Convert.ChangeType(before_check, typeof(long));

		if (before==0) {
			SceneManager.LoadScene ("AR_Scene");
		} else {
			if (now >= before_plus) {
				SceneManager.LoadScene ("AR_Scene");
			} else {
				DateTime start = Convert.ToDateTime (DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss"));
				DateTime end = Convert.ToDateTime (before_);
				TimeSpan TimeDiff = end - start;
				panel.SetActive (true);
				GameObject text = GameObject.Find ("wait_Text");
				text.GetComponent<Text> ().text = TimeDiff.Seconds+"초 후 다시 시도해주세요.";
				Invoke ("Destroy_window",2);
			}
		}

	}
	public void Delay_Time(){
		string now_time = System.DateTime.Now.ToString ("yyyyMMddHHmmss");
		string before_check = System.DateTime.Now.AddSeconds (50).ToString ("yyyyMMddHHmmss");
		string before_ = DateTime.Now.AddSeconds (50).ToString ("yyyy/MM/dd hh:mm:ss");
		string before_string = now_time;
		PlayerPrefs.SetString ("before_string",before_string);
		PlayerPrefs.SetString ("before_check",before_check);
		PlayerPrefs.SetString ("before_",before_);
	}
	void Destroy_window(){
		GameObject panel = GameObject.Find ("Wait_panel");
		panel.SetActive (false);
	}
	public void SceneChange_Main(){
		SceneManager.LoadScene ("Basic");
	}
}