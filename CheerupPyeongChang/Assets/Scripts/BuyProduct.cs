﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class BuyProduct : MonoBehaviour {
	public GameObject panel;
	public GameObject canvas;
	static int price_value;
	GameObject point_panel;

	public void ClickButton(GameObject target){
		string what_item = target.name;
		GameObject item = GameObject.Find (what_item+"_price");
		string getprice = item.GetComponent<Text> ().text;
		GameObject text = GameObject.Find ("canvas_Text");
		string[] price = getprice.Split ('p');
		price_value = (int)Convert.ChangeType(price[0], typeof(int));
		text.GetComponent<Text>().text = price_value+"P가 차감됩니다. \n구매하시겠습니까?";


	}
	public void Buy(){
		
		if (Point.point >= price_value) {
			PlayerPrefs.SetInt("point", Point.point- price_value);
			Point.point = PlayerPrefs.GetInt ("point", 0);
			canvas.SetActive (false);
		} else {
			canvas.SetActive (false);
			GameObject Canvas_panel = GameObject.Find ("Canvas");
			point_panel = MonoBehaviour.Instantiate (panel) as GameObject;
			point_panel.transform.parent = Canvas_panel.transform;
			point_panel.transform.localPosition = new Vector3 (0,0,0);
			point_panel.transform.localScale = new Vector3 (2,2,2);
			GameObject image = GameObject.Find ("Panel_Image");
			image.transform.localScale = new Vector3 (0.5f,0.5f,0.5f);
			point_panel.SetActive (true);
			Invoke ("Destroy_window",2);
		}
	}
	void Destroy_window(){
		Destroy (point_panel.gameObject);

	}
	public void Cancel(){
		canvas.SetActive (false);
	}
}
