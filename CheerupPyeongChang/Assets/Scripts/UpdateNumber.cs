﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class UpdateNumber : MonoBehaviour {
	public GameObject check;
	static GameObject checking;

	public void Cheer_check(){
		GameObject Canvas_panel = GameObject.Find ("Canvas");
		checking = MonoBehaviour.Instantiate (check) as GameObject;
		checking.transform.parent = Canvas_panel.transform;
		checking.transform.localPosition = new Vector3 (0,0,0);
		checking.transform.localScale = new Vector3(2,2,2);
		GameObject image = GameObject.Find ("Cheerup_check");
		image.transform.localScale = new Vector3 (0.7f,0.7f,0.7f);
		checking.SetActive (true);
		GameObject text = GameObject.Find ("cheer_Text");
		text.GetComponent<Text>().text = "100P가 차감됩니다. 응원하시겠습니까?";

		cheerup cheerup = checking.GetComponent<cheerup> ();
		string ObjectName = transform.name;
		string[] id_string;
		id_string = ObjectName.Split ('_');
		cheerup.id = (int)Convert.ChangeType(id_string[1], typeof(int));
		cheerup.checking = checking;

	}

	public void Cancel_cheer(){
		Destroy (checking.gameObject);
	}

}
