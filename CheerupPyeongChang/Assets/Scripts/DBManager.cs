﻿using UnityEngine;
using System.Collections;
using System;
using System.Data;
using Mono.Data.Sqlite;
using System.IO;
using UnityEngine.UI;

public class DBManager : MonoBehaviour {
	
	// Use this for initialization
	void Start() {
		string conn = Application.persistentDataPath+"/User.db";
		Debug.Log (conn);
		if(!File.Exists( conn ) ) 
		{ 
			WWW wwwUrl = new WWW("jar:file://"+ Application.dataPath+ "!/assets/User.db"); 
			while (!wwwUrl.isDone) {}
			File.WriteAllBytes( conn, wwwUrl.bytes ); 
			IDbConnection dbconn;
			dbconn = (IDbConnection)new SqliteConnection ("URI=file:" + conn);
			dbconn.Open ();

			IDbCommand dbcmd = dbconn.CreateCommand ();
			string sqlQuery;
			sqlQuery = "CREATE TABLE card_info( ";
			sqlQuery += "card_id INT PRIMARY KEY\tNOT NULL";
			sqlQuery += ");";

			dbcmd.CommandText = sqlQuery;
			dbcmd.ExecuteNonQuery ();

			dbcmd.Dispose ();
			dbcmd = null;
			dbconn.Close ();
			dbconn = null;
		} 
		read_card ();
	}

	public void insert_card(int point_num){
		string conn = Application.persistentDataPath+"/User.db";
		IDbConnection dbconn;
		dbconn = (IDbConnection)new SqliteConnection ("URI=file:" + conn);
		dbconn.Open ();

		IDbCommand dbcmd = dbconn.CreateCommand ();
		string sqlQuery;
		sqlQuery = "INSERT INTO card_info ";
		sqlQuery += "VALUES("+point_num+")";

		dbcmd.CommandText = sqlQuery;
		dbcmd.ExecuteNonQuery ();

		dbcmd.Dispose ();
		dbcmd = null;
		dbconn.Close ();
		dbconn = null;

	}

	public void read_card(){
		GameObject card = GameObject.Find ("card");
		if (card != null) {
			ArrayList cards = new ArrayList ();
			string conn = Application.persistentDataPath+"/User.db";
			IDbConnection dbconn;
			dbconn = (IDbConnection)new SqliteConnection ("URI=file:" + conn);
			dbconn.Open ();

			IDbCommand dbcmd = dbconn.CreateCommand ();
			string sqlQuery;
			sqlQuery = "SELECT * FROM card_info";
			dbcmd.CommandText = sqlQuery;
			IDataReader reader = dbcmd.ExecuteReader ();

			while (reader.Read ()) {
				int id = reader.GetInt16 (0);
				cards.Add (id);
			}

			GameObject count = card.transform.FindChild ("Text").gameObject;
			count.GetComponent<Text> ().text = cards.Count + "";

			reader.Close ();
			reader = null;
			dbcmd.Dispose ();
			dbcmd = null;
			dbconn.Close ();
			dbconn = null;

			GetCards (cards);
		}
	}
	public void GetCards(ArrayList cards){
		for(int i=0; i<cards.Count; i++){
			int card_num = (int)Convert.ChangeType(cards [i], typeof(int));
			GameObject card = GameObject.Find ("CardItem_"+card_num+"_question");
			card.GetComponent<Image> ().sprite = Resources.Load<Sprite>("Image/id_"+card_num);
			card.name = "CardItem_"+card_num+"_real";
		}
	}
}

