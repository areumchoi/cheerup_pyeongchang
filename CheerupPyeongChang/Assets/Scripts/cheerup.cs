﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class cheerup : MonoBehaviour {
	public GameObject panel;
	public int id=0;
	public GameObject checking;
	GameObject point_panel;


	public void Search_id(){
		if (Point.point >= 100) {
			StartCoroutine (SetNumber (id));
			PlayerPrefs.SetInt("point", Point.point- 100);
			Point.point = PlayerPrefs.GetInt ("point", 0);
		} else {
			checking.SetActive (false);
			GameObject Canvas_panel = GameObject.Find ("Canvas");
			point_panel = MonoBehaviour.Instantiate (panel) as GameObject;
			point_panel.transform.parent = Canvas_panel.transform;
			point_panel.transform.localPosition = new Vector3 (0,0,0);
			point_panel.transform.localScale = new Vector3 (2,2,2);
			GameObject image = GameObject.Find ("Panel_Image");
			image.transform.localScale = new Vector3 (0.5f,0.5f,0.5f);
			point_panel.SetActive (true);
			Invoke ("Destroy_window",2);

		}
	}
	void Destroy_window(){
		Destroy (point_panel.gameObject);
		Destroy (checking.gameObject);
	}
	IEnumerator SetNumber(int clicked_id){
		WWWForm form = new WWWForm ();
		form.AddField ("id_post", clicked_id);

		WWW www = new WWW ("https://cheeruppyeongchang.000webhostapp.com/SearchID.php", form);
		yield return www;
		string up_number_string = www.text;
		int up_number = (int)Convert.ChangeType(up_number_string, typeof(int));

		StartCoroutine (UpdateUpNum(clicked_id,up_number+1));
	}
	IEnumerator UpdateUpNum(int clicked_id, int new_up){
		WWWForm form = new WWWForm ();
		form.AddField ("newid_post", clicked_id);
		form.AddField ("up_post", new_up);

		WWW www = new WWW ("https://cheeruppyeongchang.000webhostapp.com/UpdateData.php", form);
		yield return www;

		Destroy (checking.gameObject);

		GameObject Panel_rank = GameObject.Find ("Panel_rank");
		Panel_rank.GetComponent<LoadRanking> ().StartLoad();
	}

}
