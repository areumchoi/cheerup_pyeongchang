﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class Card_Information : MonoBehaviour {
	public GameObject panel_info;
	public void Card_Clicked(){
		GameObject card = this.gameObject;
		string[] id_string;
		id_string = card.name.Split ('_');
		int clicked_id = (int)Convert.ChangeType(id_string[1], typeof(int));
		if (id_string[2].Equals("real")) {
			panel_info.SetActive (true);
			GameObject card_info = GameObject.Find ("Card_info");
			card_info.GetComponent<Image> ().sprite = Resources.Load<Sprite>("Image/id_"+clicked_id+"_intro");
		}
	}

	public void Card_info_Clicked(){
		GameObject Panel = GameObject.Find ("Panel_info");
		Panel.SetActive (false);
	}
	public void Panel_Clicked(){
		GameObject Panel = GameObject.Find ("Panel_info");
		Panel.SetActive (false);
	}
}
