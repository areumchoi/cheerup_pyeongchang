﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GetBox : MonoBehaviour {

	public Text text;
	public GameObject ImageTarget1;
	public GameObject ImageTarget2;
	public GameObject ImageTarget3;


	void Start () {
		int point = Random.Range (10, 50);
		int point_num = Random.Range (1,26);
		PlayerPrefs.SetInt("point", Point.point + point);
		Point.point = PlayerPrefs.GetInt ("point", 0);
		text.GetComponent<Text> ().text = point + "";

		GameObject card = GameObject.Find ("CardItem");
		card.GetComponent<Image> ().sprite = Resources.Load<Sprite>("Image/id_"+point_num);
		DBManager db = new DBManager ();
		db.insert_card (point_num);
		ChangeScene delay = new ChangeScene ();
		delay.Delay_Time ();

		ImageTarget1.SetActive (false);
		ImageTarget2.SetActive (false);
		ImageTarget3.SetActive (false);
	}
}