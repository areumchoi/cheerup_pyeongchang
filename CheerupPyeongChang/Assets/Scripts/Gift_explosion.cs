﻿using UnityEngine;
using System.Collections;

public class Gift_explosion : MonoBehaviour {
	Animator anim;
	public Ray ray;
	public RaycastHit hitInfo;
	public GameObject gift_1;
	public GameObject firecracker_1;
	public GameObject UI;

	void Start(){
		anim = gift_1.GetComponent<Animator> ();
	}
	void Update(){
		ray=Camera.main.ScreenPointToRay(Input.mousePosition );

		if(Input.GetMouseButtonDown(0)){
			if(Physics.Raycast(ray, out hitInfo)){ 
				anim.SetBool ("Clicking",true);
				StartCoroutine ("Waiting");

			}
		}
	}
	IEnumerator Waiting(){
		yield return new WaitForSeconds (2);
		anim.SetTrigger ("Explosion");
		gift_1.SetActive (false);
		firecracker_1.SetActive (true);
		yield return new WaitForSeconds (1);
		firecracker_1.SetActive (false);
		UI.SetActive (true);
	}
}
