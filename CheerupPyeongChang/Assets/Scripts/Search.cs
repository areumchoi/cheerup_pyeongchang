﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class Search : MonoBehaviour {
	public string[] items;
	GameObject Content;
	Transform[] childList;
	public GameObject loading;

	public void clean_input(InputField input){
		if (input != null) {
			input.text = "";
		}
	}

	public void OnSearch(InputField input){		
		
		Content = GameObject.Find ("Content_rank");
		childList = Content.GetComponentsInChildren<Transform>(true); 
		if (input.text.Equals("")) {
			for (int i = 1; i < childList.Length; i++) {
				childList[i].gameObject.SetActive (true);
			}
		} else {
			loading.SetActive (true);
			for (int i = 1; i < childList.Length; i++) {
				childList[i].gameObject.SetActive (false);
			}
			StartCoroutine (SearchPHP (input.text));
		}

	}
	IEnumerator SearchPHP(string search_data){
		WWWForm form = new WWWForm ();
		form.AddField ("content_post", search_data);

		WWW SelectedData = new WWW ("https://cheeruppyeongchang.000webhostapp.com/SearchData.php", form);
		yield return SelectedData;
		string SelectedString = SelectedData.text;
		items = SelectedString.Split (';');

		for (int i = 0; i < items.Length - 1; i++) {
			int id = (int)Convert.ChangeType(items[i], typeof(int));
			GameObject player = Content.transform.FindChild ("player_"+id).gameObject;
			Transform[] player_child = player.GetComponentsInChildren<Transform> (true);
			player.SetActive (true);
			for(int j=1; j<player_child.Length; j++){
				player_child [j].gameObject.SetActive (true);
			}
		}
		loading.SetActive (false);
	}
}