﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class LoadRanking : MonoBehaviour {

	public string[] items;
	public GameObject loading;
	public GameObject input;

	public void StartLoad(){
		GameObject Content = GameObject.Find ("Content_rank");
		loading.SetActive (true);
		input.SetActive (false);
		if (Content!=null) {
			Transform[] childList = Content.GetComponentsInChildren<Transform>(true); 
			if(childList != null) 
			{ 
				print (childList.Length);
				for(int i=1;i<childList.Length;i++) 
				{ 
					if(childList[i] != transform) 
						Destroy(childList[i].gameObject); 
				} 
			} 
			StartCoroutine (Load());
		}
	}

	IEnumerator Load () {
		WWW SelectedData = new WWW ("https://cheeruppyeongchang.000webhostapp.com/SelectData.php");
		yield return SelectedData;
		string SelectedString = SelectedData.text;
		int rank = 1;
		int compare = 1;
		items = SelectedString.Split (';');
		for (int i = 0; i < items.Length-1; i++) {
			string id_string = GetDataValue (items[i],"ID:");
			int id = (int)Convert.ChangeType(id_string, typeof(int));
			string player_name = GetDataValue (items[i], "Name:");
			string nation = GetDataValue (items[i],"Nation:");
			string field = GetDataValue (items[i],"Field:");
			string up_number_string = GetDataValue (items[i],"Up_number:");
			int up_number = (int)Convert.ChangeType(up_number_string, typeof(int));
			if (i == 0) {
				compare = up_number;
			}
			if (compare > up_number) {
				rank++;
				compare = up_number;
			}


			GameObject Content = GameObject.Find ("Content_rank");
			GameObject player_prefab = Resources.Load ("Prefabs/Player") as GameObject;
			GameObject new_player = MonoBehaviour.Instantiate (player_prefab) as GameObject;
			new_player.name = "player_"+id;
			new_player.transform.parent = Content.transform;
			new_player.transform.localScale = Vector3.one;

			GameObject Player = new_player.transform.FindChild ("Player").gameObject;
			Player.GetComponent<Image> ().sprite = Resources.Load<Sprite> ("Player/player" + id);
			GameObject Name = new_player.transform.FindChild ("player_name").gameObject;
			Name.GetComponent<Text> ().text = player_name;
			GameObject Sports = new_player.transform.FindChild ("player_sports").gameObject;
			Sports.GetComponent<Text> ().text = field + ", " + nation;
			GameObject Up_num = new_player.transform.FindChild ("player_up").gameObject;
			Up_num.GetComponent<Text> ().text = up_number_string;
			GameObject ranking = new_player.transform.FindChild ("rank").gameObject;
			ranking.GetComponent<Text> ().text = rank+"";
		}
		loading.SetActive (false);
		input.SetActive (true);
	}
	string GetDataValue(string data, string index){
		string value = data.Substring (data.IndexOf(index)+index.Length);
		if (value.Contains ("|"))
			value = value.Remove (value.IndexOf("|"));
		return value;
	}
}