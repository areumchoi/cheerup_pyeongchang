﻿using UnityEngine;
using System.Collections;

public class Vuforia_Backstep : MonoBehaviour {

	void Update () {
		if (Application.platform == RuntimePlatform.Android) {
			if (Input.GetKeyUp (KeyCode.Escape)) {
				ChangeScene change = new ChangeScene ();
				change.SceneChange_Main ();
				return;
			}
		}
	}

	public void BacktoMain(){
		ChangeScene change = new ChangeScene ();
		change.SceneChange_Main ();
	}

}
