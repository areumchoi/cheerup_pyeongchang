﻿using UnityEngine;
using System.Collections;

public class Application_Quit : MonoBehaviour {

	void Update () {
		if (Application.platform == RuntimePlatform.Android) {
			if (Input.GetKeyUp (KeyCode.Escape)) {
				Application.Quit ();
				return;
			}
		}
	}
}
